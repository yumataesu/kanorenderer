//
//  DeferredRenderer.hpp
//  of-DeferredRendering
//
//  Created by Yuma Taesu on 2017/08/06.
//
//
#pragma once
#include "ofMain.h"
#include "GBuffer.hpp"
#include "SSAOPass.hpp"

class DeferredShader : ofShader
{
public:
    void setup(int w, int h);
    void geometryPassBegin(ofMatrix4x4& model, ofEasyCam& cam, ofMatrix4x4& view);
    void geometryPassEnd(ofEasyCam& cam);
    void lightingPassBegin();
    void lightingPassEnd();
    
    
    void ssaoRender(ofEasyCam cam);

    
    void uniformLights();
    void uniformCam();
    
    void uniformTexture();
    
    void drawGui();
    
    ofFbo& getLightedFbo();
    
    ofShader m_GeometryPassShader, m_LightingPassShader;
    
    GBuffer* gbuffer;
    
private:
    void loadShaders();
    
    SSAOPass* ssaoPass;
    ofMatrix4x4 m_viewMatrix, m_preViewMatrix;
    ofMatrix4x4 m_preModelMatrix;
    
};
