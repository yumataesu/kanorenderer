#include <random>

#include "ofMain.h"
#include "ofxImGui.h"
#include "ofxAssimpModelLoader.h"
#include "DeferredShader.hpp"

#define NUM 100

class ofApp : public ofBaseApp{
    
    const int WIDTH = ofGetWidth();
    const int HEIGHT = ofGetHeight();
    
    ofxAssimpModelLoader lucy;
    ofVboMesh vbomesh;
    
    ofxImGui gui;
    
    ofSpherePrimitive lightsphere;
    DeferredShader deferredShader;

    ofVec3f pos[NUM];
    ofEasyCam cam;
    
    
    ofTexture texture, normal, metal, roughness;
    
    ofMesh quad;
    
    ofVec3f lightpos;
    
    

    float time;
    float lastFrame, currentFrame, deltaTime, deltaTime_;
    float delta;
    
    
    ofShader motionBlurShader;
    float velocityScale;
    
    
    //for hdr-bloom
    ofFbo pingPongFbo[2];
    ofFbo hdrFbo;
    ofShader blurShader, bloomFinalShader;
    float exposure = 1.0f;
    float bloomThreshold = 0.2;
    bool horizontal;
    
    
    bool isrecord;
    ofFbo finalOutFbo;
    
    ofImage ss;
    
    bool bgui = true;
    //--------------------------------------------------------------
    void setup()
    {
        
        gui.setup();
        
        float R = 0;
        for(int i = 0; i < NUM; i++)
        {
            pos[i] = ofVec3f(ofRandom(-R, R), 2, ofRandom(-R, R));
        }
        
        cam.setupPerspective(false, 80, 0.01f, 200.0f);
        
        deferredShader.setup(WIDTH, HEIGHT);
        
        ofDisableArbTex();
        ofLoadImage(texture, "rusted_iron/albedo.png");
        ofLoadImage(normal, "rusted_iron/normal.png");
        ofLoadImage(metal, "rusted_iron/metallic.png");
        ofLoadImage(roughness, "rusted_iron/roughness.png");
        ofEnableAntiAliasing();
        
        
        lucy.loadModel("pot.obj");
        motionBlurShader.load("shaders/motion_blur");
        blurShader.load("shaders/bloom/blur");
        bloomFinalShader.load("shaders/bloom/bloom_final");

        lightsphere.set(10, 30);
        
        
        vbomesh = lucy.getMesh(0);
        glEnable(GL_DEPTH_TEST);
        ofDisableArbTex();
        
        {
        
        vector<GLint> formats = { GL_RGBA16F, GL_RGBA16F };
        ofFbo::Settings settings;
        settings.width = WIDTH;
        settings.height = HEIGHT;
        settings.textureTarget = GL_TEXTURE_2D;
        settings.wrapModeHorizontal = GL_CLAMP_TO_EDGE;
        settings.wrapModeVertical = GL_CLAMP_TO_EDGE;
        settings.minFilter = GL_NEAREST;
        settings.maxFilter = GL_NEAREST;
        settings.numColorbuffers = 2;
        settings.colorFormats = formats;
        settings.useDepth = true;
        settings.useStencil = false;
        settings.depthStencilAsTexture = false;
        
        
        hdrFbo.allocate(settings);
        }
        {
            vector<GLint> formats = { GL_RGBA16F };
            ofFbo::Settings settings;
            settings.width = WIDTH;
            settings.height = HEIGHT;
            settings.textureTarget = GL_TEXTURE_2D;
            settings.wrapModeHorizontal = GL_CLAMP_TO_EDGE;
            settings.wrapModeVertical = GL_CLAMP_TO_EDGE;
            settings.minFilter = GL_LINEAR;
            settings.maxFilter = GL_LINEAR;
            settings.numColorbuffers = 1;
            settings.colorFormats = formats;
            settings.useDepth = true;
            settings.useStencil = false;
            settings.depthStencilAsTexture = false;
            
            pingPongFbo[0].allocate(settings);
            pingPongFbo[1].allocate(settings);
        }
        
        
        finalOutFbo.allocate(WIDTH, HEIGHT, GL_RGBA16F);
        
        ofEnableArbTex();
        
        quad.setMode(OF_PRIMITIVE_TRIANGLE_FAN);
        quad.addVertex(ofVec3f(1.0, 1.0, 0.0)); // top-right
        quad.addTexCoord(ofVec2f(1.0f, 0.0f));
        quad.addVertex(ofVec3f(1.0, -1.0, 0.0)); //bottom-right
        quad.addTexCoord(ofVec2f(1.0f, 1.0f));
        quad.addVertex(ofVec3f(-1.0, -1.0, 0.0)); //bottom-left
        quad.addTexCoord(ofVec2f(0.0f, 1.0f));
        quad.addVertex(ofVec3f(-1.0, 1.0, 0.0)); //top-left
        quad.addTexCoord(ofVec2f(0.0f, 0.0f));
        
        
    }
    
    //--------------------------------------------------------------
    void update()
    {
        time = ofGetElapsedTimef();
        
        GLfloat currentFrame = ofGetElapsedTimef();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;
        
        deltaTime_ += (deltaTime * delta);
        
        
    }
    
    //--------------------------------------------------------------
    void draw()
    {
        ofBackground(255);
        
        //lightpos = ofVec3f(0, 0, 20);
        
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        
        ofMatrix4x4 viewmatrix;
        {
            ofMatrix4x4 model;
            //model.scale(0.1, 0.1, 0.1);
            model.translate(0, 0, 0);
            model.rotate(deltaTime_ * 35.0, 0.0, 1.0, 0.0);
            
            cam.begin();
            viewmatrix = ofGetCurrentViewMatrix();
            deferredShader.geometryPassBegin(model, cam, viewmatrix);
            deferredShader.m_GeometryPassShader.setUniformTexture("colorTex", texture, 0);
            deferredShader.m_GeometryPassShader.setUniformTexture("normalTex", normal, 1);
            deferredShader.m_GeometryPassShader.setUniformTexture("metalTex", metal, 2);
            deferredShader.m_GeometryPassShader.setUniformTexture("roughTex", roughness, 3);
            
            vbomesh.draw();
            
            
            //deferredShader.m_GeometryPassShader.setUniformMatrix4f("preView", viewmatrix);
            deferredShader.geometryPassEnd(cam);
            
            viewmatrix = ofGetCurrentViewMatrix();
            cam.end();
        }
        
        
        glDisable(GL_DEPTH_TEST);
        deferredShader.ssaoRender(cam);
        
        
        
        // 2. Lighting Pass and write fbo for HDR
        // --------------------------------------------------
        hdrFbo.begin();
        hdrFbo.activateAllDrawBuffers();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        deferredShader.lightingPassBegin();
        deferredShader.m_LightingPassShader.setUniform3f("u_lightPos", ofVec3f(lightpos) * viewmatrix);
        deferredShader.m_LightingPassShader.setUniform3f("u_viewPos", cam.getPosition());
        deferredShader.m_LightingPassShader.setUniform1f("u_bloomThreshold", bloomThreshold);
        quad.draw();
        deferredShader.lightingPassEnd();
        hdrFbo.end();
        
        
        
        // 2. blur bright fragments with two-pass Gaussian Blur
        // --------------------------------------------------
        horizontal = true;
        bool first_iteration = true;
        unsigned int amount = 10;
        blurShader.begin();
        for (unsigned int i = 0; i < amount; i++)
        {
            //glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO[horizontal]);
            pingPongFbo[horizontal].begin();
            blurShader.setUniform1i("horizontal", horizontal);
            if(first_iteration) {
                blurShader.setUniformTexture("image", hdrFbo.getTexture(1), 0);
            } else {
                blurShader.setUniformTexture("image", pingPongFbo[!horizontal].getTexture(), 0);
            }
            quad.draw();
            horizontal = !horizontal;
            if (first_iteration) first_iteration = false;
            pingPongFbo[horizontal].end();
        }
        blurShader.end();
        //glBindFramebuffer(GL_FRAMEBUFFER, 0);
        
        
        
        
        bool bloom = true;
        // 3. now render floating point color buffer to 2D quad and tonemap HDR colors to default framebuffer's (clamped) color range
        // --------------------------------------------------------------------------------------------------------------------------
        finalOutFbo.begin();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        bloomFinalShader.begin();
        bloomFinalShader.setUniformTexture("scene", hdrFbo.getTexture(0), 0);
        bloomFinalShader.setUniformTexture("bloomBlur", pingPongFbo[!horizontal].getTexture(), 1);
        bloomFinalShader.setUniform1i("bloom", bloom);
        bloomFinalShader.setUniform1f("exposure", exposure);
        quad.draw();
        bloomFinalShader.end();
        finalOutFbo.end();
        
        
        // 4. motion blur pass
        // --------------------------------------------------------------------------------------------------------------------------
        motionBlurShader.begin();
        motionBlurShader.setUniformTexture("finalcolor", finalOutFbo.getTexture(), 0);
        motionBlurShader.setUniformTexture("gvelocity", deferredShader.gbuffer->getTexture(GBuffer::VELOCITY_AND_METAL), 1);
        motionBlurShader.setUniform1f("u_velocityScale", velocityScale);
        quad.draw();
        motionBlurShader.end();
        
        
        
        if(bgui)this->drawGui();
    }
    
    void drawGui()
    {
        gui.begin();
        deferredShader.drawGui();
        
        ImGui::Begin("View");
        ImGui::SliderFloat("delta T", &delta, 0.1, 100.0);
        ImGui::SliderFloat("bloomThreshold", &bloomThreshold, 0.01, 10.0);
        ImGui::SliderFloat("exposure", &exposure, 0.01, 10.0);
        
        ImGui::Text("Velocity Scale");
        ImGui::SliderFloat("velocityScale", &velocityScale, 0.1, 20.0);
        ImGui::End();
        
        ImGui::Begin("Light");
        ImGui::SliderFloat("X", &lightpos.x, -50, 50.0);
        ImGui::SliderFloat("y", &lightpos.y, -50, 50.0);
        ImGui::SliderFloat("z", &lightpos.z, -50, 50.0);
        //ImGui::SliderFloat("velocityScale", &velocityScale, 0.001, 10.0);
        ImGui::End();
        
        ImGui::Begin("Bloom");
        ImGui::ImageButton((ImTextureID)(uintptr_t) pingPongFbo[!horizontal].getTexture().getTextureData().textureID, ImVec2(256, 142));
        ImGui::End();
        
        gui.end();
        
        
        
        if(isrecord)
        {
            ss.grabScreen(0, 0, WIDTH, HEIGHT);
            string fileName = "snapshot_"+ofToString(10000+ofGetFrameNum())+".png";
            ss.save(fileName);
        }
    }
    
    //--------------------------------------------------------------
    void keyPressed(int key)
    {
        if(key == 'r')
        {
            isrecord = !isrecord;
        }
        
        if(key == 'g')
        {
            bgui = !bgui;
        }
    }
    
    //--------------------------------------------------------------
    void keyReleased(int key)
    {
        
    }
};

//========================================================================
int main()
{
    ofGLFWWindowSettings settings;
    settings.setGLVersion(3, 3);
    settings.width = 1280;
    settings.height = 720;
    settings.resizable = false;
    ofCreateWindow(settings);
    
    // this kicks off the running of my app
    // can be OF_WINDOW or OF_FULLSCREEN
    // pass in width and height too:
    ofRunApp(new ofApp());
    
}
