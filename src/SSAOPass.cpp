//
//  SSAOPass.cpp
//  of-DeferredRendering
//
//  Created by Yuma Taesu on 2017/07/27.
//
//

#include "SSAOPass.hpp"

SSAOPass::SSAOPass()
: m_SSAOFbo(NULL)
, m_SSAOBlurFbo(NULL)
, m_ssaoRadius(2.0)
{}


SSAOPass::~SSAOPass()
{}

void SSAOPass::setup(int w, int h)
{
    m_SSAOFbo = new ofFbo();
    m_SSAOBlurFbo = new ofFbo();
    
    vector<GLint> formats = { GL_R8 };
    ofFbo::Settings settings;
    settings.width = w;
    settings.height = h;
    settings.textureTarget = GL_TEXTURE_2D;
    settings.wrapModeHorizontal = GL_CLAMP_TO_EDGE;
    settings.wrapModeVertical = GL_CLAMP_TO_EDGE;
    settings.minFilter = GL_LINEAR;
    settings.maxFilter = GL_LINEAR;
    settings.numColorbuffers = 1;
    settings.colorFormats = formats;
    settings.numSamples = 4;
//    settings.useDepth = true;
//    settings.useStencil = true;
//    settings.depthStencilAsTexture = true;
    
    
    m_SSAOFbo->allocate(settings);
    m_SSAOBlurFbo->allocate(settings);
    
    m_SSAOShader.load("shaders/ssao");
    m_SSAOBlurShader.load("shaders/blur_for_ssao");
    
    noiseTexture = genNoiseTexture();
    
    quad.setMode(OF_PRIMITIVE_TRIANGLE_FAN);
    quad.addVertex(ofVec3f(1.0, 1.0, 0.0)); // top-right
    quad.addTexCoord(ofVec2f(1.0f, 0.0f));
    quad.addVertex(ofVec3f(1.0, -1.0, 0.0)); //bottom-right
    quad.addTexCoord(ofVec2f(1.0f, 1.0f));
    quad.addVertex(ofVec3f(-1.0, -1.0, 0.0)); //bottom-left
    quad.addTexCoord(ofVec2f(0.0f, 1.0f));
    quad.addVertex(ofVec3f(-1.0, 1.0, 0.0)); //top-left
    quad.addTexCoord(ofVec2f(0.0f, 0.0f));
}

GLuint SSAOPass::genNoiseTexture()
{
    // generate sample kernel
    // ----------------------
    ssaoKernel.reserve(KARNEL_SAMPLE);
    float randomFloats = ofRandom(1.0);
    for (int i = 0; i < KARNEL_SAMPLE; i++)
    {
        ofVec3f sample = ofVec3f(ofRandom(1.0) * 2.0 - 1.0, ofRandom(1.0) * 2.0 - 1.0, ofRandom(1.0));
        sample = sample.normalize();
        sample *= randomFloats;
        float scale = float(i) / 64.0;
        
        // Scale samples s.t. they're more aligned to center of kernel
        scale = lerp(0.1f, 1.0f, scale * scale);
        sample *= scale;
        ssaoKernel[i] = sample;
    }
    
    
    // generate noise texture
    // ----------------------
    GLuint noiseTexture;
    vector<ofVec3f> ssaoNoise;
    for (int i = 0; i < 16; i++)
    {
        ofVec3f noise = ofVec3f(ofRandom(1.0) * 2.0 - 1.0, ofRandom(1.0) * 2.0 - 1.0, 0.0f); // rotate around z-axis (in tangent space)
        ssaoNoise.push_back(noise);
    }
    glGenTextures(1, &noiseTexture);
    glBindTexture(GL_TEXTURE_2D, noiseTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    
    return noiseTexture;
}


void SSAOPass::renderToFbo(GBuffer& gBuffer, ofEasyCam& cam)
{
    
    //gBuffer.getTexture(GBuffer::NORMAL_AND_DEPTH).draw(0, 0);
    m_SSAOFbo->begin();
    glClear(GL_COLOR_BUFFER_BIT);
    
    m_SSAOShader.begin();
    
    m_SSAOShader.setUniformMatrix4f("projection", cam.getProjectionMatrix());
    for(int i = 0; i < KARNEL_SAMPLE; i++)
    {
        m_SSAOShader.setUniform3fv("samples["+to_string(i)+"].offset", &ssaoKernel[i][0], 1);
    }
    
    m_SSAOShader.setUniformTexture("gPosition", gBuffer.getTexture(GBuffer::POSITION), 0);
    m_SSAOShader.setUniformTexture("gNormal", gBuffer.getTexture(GBuffer::NORMAL_AND_DEPTH), 1);
    m_SSAOShader.setUniformTexture("noiseTexture", GL_TEXTURE_2D, noiseTexture, 2);
    m_SSAOShader.setUniform1f("u_radius", m_ssaoRadius);
    
    quad.draw(OF_MESH_FILL);
    
    m_SSAOShader.end();
    m_SSAOFbo->end();
    
  
    m_SSAOBlurFbo->begin();
    glClear(GL_COLOR_BUFFER_BIT);
    m_SSAOBlurShader.begin();
    m_SSAOBlurShader.setUniformTexture("ssao", m_SSAOFbo->getTexture(), 0);
    quad.draw(OF_MESH_FILL);
    m_SSAOBlurShader.end();
    m_SSAOBlurFbo->end();
    
}

void SSAOPass::draw()
{
    m_SSAOFbo->draw(0, 0);
}



void SSAOPass::drawGui()
{
    bool guiShow = true;
    ImGui::Begin("AO");
    ImGui::ImageButton((ImTextureID)(uintptr_t) m_SSAOBlurFbo->getTexture(0).getTextureData().textureID, ImVec2(256, 142));
    ImGui::SliderFloat("Radius", &m_ssaoRadius, 0.1f, 50.0f);
    ImGui::End();
}



