#version 330 core

struct Light
{
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
const int LightNUM = 3;


uniform vec3 u_lightPos;
uniform sampler2D gPosition;
uniform sampler2D gNormal_And_Depth;
uniform sampler2D gAlbedo;
uniform sampler2D gVelocity_And_Metal;
uniform sampler2D gRoughness;
uniform sampler2D ssao;
uniform vec3 viewPos;
uniform float u_velocityScale = 2.5;
uniform float u_bloomThreshold;

uniform int DebugMode;

in vec2 v_texcoord;

layout (location = 0) out vec4 fragColor;
layout (location = 1) out vec4 hdrfragColor;


const float PI = 3.14159265359;
const int MAX_SAMPLES = 64;

// ----------------------------------------------------------------------------
// Easy trick to get tangent-normals to world-space to keep PBR code simplified.
// Don't worry if you don't get what's going on; you generally want to do normal
// mapping the usual way for performance anways; I do plan make a note of this
// technique somewhere later in the normal mapping tutorial.
// ----------------------------------------------------------------------------
float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
    
    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
    
    return nom / denom;
}
// ----------------------------------------------------------------------------
float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;
    
    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
    
    return nom / denom;
}
// ----------------------------------------------------------------------------
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);
    
    return ggx1 * ggx2;
}
// ----------------------------------------------------------------------------
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}


void main()
{
    
    vec3 albedo = pow(texture(gAlbedo, v_texcoord).rgb, vec3(2.2));
    
    vec3 position = texture(gPosition, v_texcoord).xyz;
    vec3 normal = texture(gNormal_And_Depth, v_texcoord).xyz;
    //vec3 albedo = pow(texture(gAlbedo, v_texcoord).rgb, vec3(2.2));
    
    
    float metallic = texture(gVelocity_And_Metal, v_texcoord).b;
    float ao = texture(ssao, v_texcoord).r;
    float roughness = texture(gRoughness, v_texcoord).r;
    
    
    //Culculate PBR
    vec3 N = normalize(normal);
    vec3 V = normalize(viewPos - position);
    vec3 R = reflect(-V, normal);
    
    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0
    // of 0.04 and if it's a metal, use their albedo color as F0 (metallic workflow)
    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo.rgb, vec3(metallic, metallic, metallic));
    
    // reflectance equation
    vec3 Lo = vec3(0.0, 0.0, 0.0);
    

    // calculate per-light radiance
    vec3 L = normalize(u_lightPos - position);
    vec3 H = normalize(V + L);
    float distance = length(u_lightPos - position) * 0.02;;
    float attenuation = 1.0 / (distance * distance);
    vec3 radiance = vec3(0.5) * attenuation;
    
    // Cook-Torrance BRDF
    float NDF = DistributionGGX(N, H, roughness);
    float G   = GeometrySmith(N, V, L, roughness);
    vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);
    
    vec3 nominator    = NDF * G * F * 2.0;
    float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; // 0.001 to prevent divide by zero.
    vec3 specular = nominator / denominator;
    
    // kS is equal to Fresnel
    vec3 kS = F;
    // for energy conservation, the diffuse and specular light can't
    // be above 1.0 (unless the surface emits light); to preserve this
    // relationship the diffuse component (kD) should equal 1.0 - kS.
    vec3 kD = vec3(1.0, 1.0, 1.0) - kS;
    // multiply kD by the inverse metalness such that only non-metals
    // have diffuse lighting, or a linear blend if partly metal (pure metals
    // have no diffuse light).
    kD *= 1.0 - metallic;
    
    // scale light by NdotL
    float NdotL = max(dot(N, L), 0.0);
    
    // add to outgoing radiance Lo
    Lo += (kD * albedo / PI + specular) * radiance * NdotL;  // note that we already multiplied the BRDF by the Fresnel (kS) so we won't multiply by kS again
    
    
    vec3 ambient = (vec3(0.06) * albedo);
    
    vec3 color = (ambient + (Lo * 1.0)) * vec3(ao, ao, ao);
    
    // HDR tonemapping
    color = color / (color + vec3(1.0));
    // gamma correct
    color = pow(color, vec3(1.0 / 2.2));
    
    
//    fragColor = vec4(AmbientOcculusion, AmbientOcculusion, AmbientOcculusion, 1.0);
    fragColor = vec4(color, 1.0);
    
    float brightness = dot(color, vec3(0.2126, 0.7152, 0.0722));
    if(brightness > u_bloomThreshold)
    {
        hdrfragColor = vec4(color, 1.0);
    } else {
        hdrfragColor = vec4(0.0, 0.0, 0.0, 1.0);
    }
}
