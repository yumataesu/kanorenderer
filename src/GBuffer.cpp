//
//  GBuffer.cpp
//  of-DeferredRendering
//
//  Created by Yuma Taesu on 2017/08/05.
//
//

#include "GBuffer.hpp"

GBuffer::GBuffer(){}
GBuffer::~GBuffer(){}

void GBuffer::createGBuffer(int w, int h)
{
    
    vector<GLint> formats = { GL_RGBA16F, GL_RGBA, GL_RGBA16F, GL_RGBA16F, GL_R8 };
    ofFbo::Settings settings;
    settings.width = w;
    settings.height = h;
    settings.textureTarget = GL_TEXTURE_2D;
    settings.wrapModeHorizontal = GL_CLAMP_TO_EDGE;
    settings.wrapModeVertical = GL_CLAMP_TO_EDGE;
    settings.minFilter = GL_LINEAR;
    settings.maxFilter = GL_LINEAR;
    settings.numColorbuffers = 5;
    settings.colorFormats = formats;
    settings.useDepth = true;
    settings.useStencil = false;
    settings.depthStencilAsTexture = false;
    
    m_fbo.allocate(settings);
    m_fbo.checkStatus();
    
}

void GBuffer::beginMRT()
{
    m_fbo.begin();
    m_fbo.activateAllDrawBuffers();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


void GBuffer::end()
{
    ofClearAlpha();
    m_fbo.end();
}


ofTexture& GBuffer::getTexture(GBUFFER_TYPE gbufferType)
{
    switch(gbufferType){
        case GBuffer::POSITION:
            return m_fbo.getTexture(0);
            break;
            
        case GBuffer::NORMAL_AND_DEPTH:
            return m_fbo.getTexture(1);
            break;
            
        case GBuffer::ALBEDO:
            return m_fbo.getTexture(2);
            break;
            
        case GBuffer::VELOCITY_AND_METAL:
            return m_fbo.getTexture(3);
            break;
        
        case GBuffer::ROUGHNESS:
            return m_fbo.getTexture(4);
            break;
        
        default:
            ofLog(OF_LOG_WARNING, "You get wrong texture!");
            break;
    }
}
