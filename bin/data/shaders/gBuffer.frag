#version 330 core

in vec3 v_world;
in vec3 v_normal;
in vec2 v_texcoord;
in vec3 v_preworld;

layout (location = 0) out vec4 gPosition;
layout (location = 1) out vec4 gNormal;
layout (location = 2) out vec4 gAlbedo;
layout (location = 3) out vec4 gVelocity;
layout (location = 4) out vec4 grough;


uniform sampler2D colorTex;
uniform sampler2D normalTex;
uniform sampler2D metalTex;
uniform sampler2D roughTex;

const float NEAR = 0.01f; // Projection matrix's near plane distance
const float FAR = 270.0f; // Projection matrix's far plane distance

//http://www.thetenthplanet.de/archives/1180
mat3 cotangent_frame(vec3 N, vec3 p, vec2 uv)
{
    // get edge vectors of the pixel triangle
    vec3 dp1 = dFdx( p );
    vec3 dp2 = dFdy( p );
    vec2 duv1 = dFdx( uv );
    vec2 duv2 = dFdy( uv );
    
    // solve the linear system
    vec3 dp2perp = cross( dp2, N );
    vec3 dp1perp = cross( N, dp1 );
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;
    
    // construct a scale-invariant frame
    float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
    return mat3( T * invmax, B * invmax, N );
}

float LinearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0; // Back to NDC
    return (2.0 * NEAR * FAR) / (FAR + NEAR - z * (FAR - NEAR));
}

void main()
{
    vec3 normal =  texture(normalTex, v_texcoord).rgb;
    mat3 tanM = cotangent_frame(v_normal, v_world, v_texcoord);
    normal = normalize(tanM * normal);
    
    float depth = LinearizeDepth(gl_FragCoord.z);
    vec4 albedo = texture(colorTex, v_texcoord);
    
    
    //culculate velocity
    vec2 a = (v_world.xy / v_world.z) * 0.5 + 0.5;
    vec2 b = (v_preworld.xy / v_preworld.z) * 0.5 + 0.5;
    vec2 vel = a - b;
    
    
    float metal = texture(metalTex, v_texcoord).r;
    float roughness = texture(roughTex, v_texcoord).r;
    
    gPosition = vec4(v_world, 1.0);
    gNormal = vec4(normal, 1.0);
    gAlbedo = vec4(albedo.rgb, 1.0);
    gVelocity = vec4(vel, metal, 1.0);
    grough = vec4(roughness, roughness, roughness, 1.0);
}
