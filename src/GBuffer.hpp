//
//  GBuffer.hpp
//  of-DeferredRendering
//
//  Created by Yuma Taesu on 2017/08/05.
//
//

#pragma once
#include "ofMain.h"
#include "ofxImGui.h"

class GBuffer : ofFbo {

public:
    enum GBUFFER_TYPE {
        POSITION,
        NORMAL_AND_DEPTH,
        ALBEDO,
        VELOCITY_AND_METAL,
        ROUGHNESS
    };
    
    
    GBuffer();
    ~GBuffer();
    
    void createGBuffer(int w, int h);
    void beginMRT();
    void end();
    ofTexture& getTexture(GBUFFER_TYPE gbufferType);
    ofFbo& getFbo() { return m_fbo; }
    
protected:
    ofFbo m_fbo;
};
