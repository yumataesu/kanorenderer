#version 330 core

const int kernelSize = 64;

struct Sample
{
    vec3 offset;
};

uniform Sample samples[kernelSize];
uniform float u_radius = 2.0;

uniform sampler2D gNormal;
uniform sampler2D gPosition;
uniform sampler2D noiseTexture;
uniform mat4 projection;

in vec2 v_texcoord;

out vec4 fragColor;

const vec2 noiseScale = vec2(1280.0f / 4.0f, 720.0f / 4.0f);
float bias = 0.025;

void main()
{
    vec4 position = texture(gPosition, v_texcoord);
    vec3 normal = texture(gNormal, v_texcoord).xyz;
    vec3 randomVec = texture(noiseTexture, v_texcoord * noiseScale).xyz;
    
    vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
    vec3 bitangent = cross(normal, tangent);
    mat3 TBN = mat3(tangent, bitangent, normal);
    
    float occlusion = 0.0;
    vec3 sample = vec3(0.0);
    
    for(int i = 0; i < kernelSize; ++i)
    {
        // get sample position
        sample = TBN * samples[i].offset; // from tangent to view-space
        sample = position.xyz + sample * u_radius;
        
        // project sample position (to sample texture) (to get position on screen/texture)
        vec4 offset = vec4(sample, 1.0);
        offset = projection * offset; // from view to clip-space
        offset.xyz /= offset.w; // perspective divide
        offset.xyz = offset.xyz * 0.5 + 0.5; // transform to range 0.0 - 1.0
        
        // get sample depth
        float sampleDepth = texture(gPosition, offset.xy).z; // get depth value of kernel sample
        
        // range check & accumulate
        float rangeCheck = smoothstep(0.0, 1.0, u_radius / abs(position.z - sampleDepth));
        occlusion += (sampleDepth >= sample.z + bias ? 1.0 : 0.0) * rangeCheck;
    }
    occlusion = 1.0 - (occlusion / kernelSize);
    
    
    //color = vec4(offset.xyz, 1.0);
    fragColor = vec4(occlusion, occlusion, occlusion, 1.0);
}
