#version 330 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

//uniform mat4 modelViewProjectionMatrix;
//uniform mat4 modelViewMatrix;
//uniform mat4 projectionMatrix;
//uniform mat4 viewMatrix;

uniform mat4 preViewMatrix;
uniform mat4 preModelMatrix;


in vec3 position;
in vec3 normal;
in vec2 texcoord;

out vec3 v_world;
out vec3 v_normal;
out vec2 v_texcoord;
out vec3 v_preworld;


void main()
{
    
    v_world =             (view *          model * vec4(position, 1.0)).xyz;
    v_preworld = (preViewMatrix * preModelMatrix * vec4(position, 1.0)).xyz;
    
    v_texcoord = texcoord;
    
    mat3 normalMatrix = transpose(inverse(mat3(view * model)));
    v_normal = normalMatrix * normal;
    
    //v_normal = normalize((view * model * vec4(normal, 0.0)).xyz);
    
    
    gl_Position = projection * view * model * vec4(position, 1.0);
}
