//
//  DeferredShader.cpp
//  of-DeferredRendering
//
//  Created by Yuma Taesu on 2017/08/06.
//
//

#include "DeferredShader.hpp"

void DeferredShader::setup(int w, int h)
{
    this->loadShaders();
    
    ofDisableArbTex();
    gbuffer = new GBuffer();
    gbuffer->createGBuffer(w, h);
    
    ssaoPass = new SSAOPass();
    ssaoPass->setup(w * 0.5, h * 0.5);
    ofEnableArbTex();
    
}

void DeferredShader::loadShaders()
{
    m_GeometryPassShader.load("shaders/gBuffer");
    m_LightingPassShader.load("shaders/lighting");
}

void DeferredShader::geometryPassBegin(ofMatrix4x4& model, ofEasyCam& cam, ofMatrix4x4& view)
{
    m_LightingPassShader.load("shaders/lighting");
    
    m_GeometryPassShader.begin();
    m_GeometryPassShader.setUniformMatrix4f("projection", cam.getProjectionMatrix());
    m_GeometryPassShader.setUniformMatrix4f("view", view);
    m_GeometryPassShader.setUniformMatrix4f("model", model);
    
    
    m_GeometryPassShader.setUniformMatrix4f("preViewMatrix", m_preViewMatrix);
    m_GeometryPassShader.setUniformMatrix4f("preModelMatrix", m_preModelMatrix);
    
    gbuffer->beginMRT();
    
    m_preViewMatrix = view;
    m_preModelMatrix = model;
}

void DeferredShader::geometryPassEnd(ofEasyCam& cam)
{
    gbuffer->end();
    m_GeometryPassShader.end();
}

ofFbo& DeferredShader::getLightedFbo()
{
    
}

void DeferredShader::ssaoRender(ofEasyCam cam)
{
    ssaoPass->renderToFbo(*gbuffer, cam);
}


void DeferredShader::lightingPassBegin()
{
    m_LightingPassShader.begin();
    m_LightingPassShader.setUniformTexture("gPosition",             gbuffer->getTexture(GBuffer::POSITION),               0);
    m_LightingPassShader.setUniformTexture("gNormal_And_Depth",     gbuffer->getTexture(GBuffer::NORMAL_AND_DEPTH),       1);
    m_LightingPassShader.setUniformTexture("gAlbedo", gbuffer->getTexture(GBuffer::ALBEDO),   2);
    m_LightingPassShader.setUniformTexture("gVelocity_And_Metal",   gbuffer->getTexture(GBuffer::VELOCITY_AND_METAL),     3);
    m_LightingPassShader.setUniformTexture("gRoughness",   gbuffer->getTexture(GBuffer::ROUGHNESS),     4);
    m_LightingPassShader.setUniformTexture("ssao", ssaoPass->getSSAOFbo().getTexture(0), 5);
}

void DeferredShader::lightingPassEnd()
{
    m_LightingPassShader.end();
}

void DeferredShader::drawGui()
{
//    bool guiShow = true;
    
    ImGui::Begin("GBuffer");
    ImGui::Text("Position");
    ImGui::ImageButton((ImTextureID)(uintptr_t) gbuffer->getTexture(GBuffer::POSITION).getTextureData().textureID, ImVec2(256, 142));
    
    ImGui::Text("Normal");
    ImGui::ImageButton((ImTextureID)(uintptr_t) gbuffer->getTexture(GBuffer::NORMAL_AND_DEPTH).getTextureData().textureID, ImVec2(256, 142));
    
    ImGui::Text("Albedo");
    ImGui::ImageButton((ImTextureID)(uintptr_t) gbuffer->getTexture(GBuffer::ALBEDO).getTextureData().textureID, ImVec2(256, 142));
    
    ImGui::Text("Velocity");
    ImGui::ImageButton((ImTextureID)(uintptr_t) gbuffer->getTexture(GBuffer::VELOCITY_AND_METAL).getTextureData().textureID, ImVec2(256, 142));
    
    ssaoPass->drawGui();
    
    ImGui::End();
}
