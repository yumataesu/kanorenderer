//
//  SSAOPass.hpp
//  of-DeferredRendering
//
//  Created by Yuma Taesu on 2017/07/27.
//
//

#pragma once

#include "ofMain.h"
#include "GBuffer.hpp"
#include "ofxImGui.h"

class SSAOPass {
public:
    
    SSAOPass();
    ~SSAOPass();
    void setup(int w, int h);
    void renderToFbo(GBuffer& gBuffer, ofEasyCam& cam);
    
    ofFbo& getSSAOFbo(){ return *m_SSAOBlurFbo; };

    void drawGui();
    void draw();
    
private:
    GLuint genNoiseTexture();
    float lerp(float a, float b, float f)
    {
        return a + f * (b - a);
    }
    
protected:

    ofFbo* m_SSAOFbo;
    ofFbo* m_SSAOBlurFbo;
    
    ofShader m_SSAOShader;
    ofShader m_SSAOBlurShader;
    
    const int KARNEL_SAMPLE = 64;
    vector<ofVec3f> ssaoKernel;
    
    GLuint noiseTexture;
    ofMesh quad;
    
    //parameters
    float m_ssaoRadius;
};
