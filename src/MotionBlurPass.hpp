//
//  MotionBlurPass.hpp
//  of-DeferredRendering
//
//  Created by Yuma Taesu on 2017/08/07.
//
//

#pragma once

#include "ofMain.h"
#include "GBuffer.hpp"
#include "ofxImGui.h"

class MotionBlurPass {
public:
    
    MotionBlurPass();
    ~MotionBlurPass();
    void setup(int w, int h);
    void renderToFbo(GBuffer& gBuffer, ofEasyCam& cam);
    
    void drawGui();
    
private:
    ofShader m_motionBlurShader;
    
protected:
    
    ofFbo m_motionBlurFbo;
    ofMesh quad;
};

